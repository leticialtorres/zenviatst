FROM node

RUN yarn global add create-react-app

ENTRYPOINT /bin/bash -c "cd /app/zenvia-test-app && yarn install && yarn start"
