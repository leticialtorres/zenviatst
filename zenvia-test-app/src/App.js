import React, { Component } from 'react';
import _ from 'lodash';
import {
  Panel,
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  Table
 } from 'react-bootstrap';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      numberQuantity: 0,
    };
    this.inputHandler = this.inputHandler.bind(this)
  }

  inputHandler(args){
    this.setState({
      numberQuantity: args.target.value
    });
  }

  fizzBuzz(numberQuantity) {
    let arrayOfTranslations = _.times(numberQuantity, index => {

      let indexPlusOne = index + 1;

      if (indexPlusOne % 3 == 0 && indexPlusOne % 5 == 0) {
          return 'FizzBuzz';
      } else if (indexPlusOne % 3 == 0) {
          return 'Fizz';
      } else if (indexPlusOne % 5 == 0) {
          return 'Buzz';
      }
      return indexPlusOne;
    });

    return arrayOfTranslations;
  }

  render() {

    let rows = _.map(this.fizzBuzz(this.state.numberQuantity), function(item, index) {
      return (<tr key={index}>
        <td>
          {item}
        </td>
      </tr>);
    })

    return (
      <Grid>
        <Row>
          <Col xs={12} md={12}>
            <Panel>
              <form>
                <FormGroup>
                  <ControlLabel>Insira a quantidade de Números para FizzBuzz</ControlLabel>
                  <FormControl
                    type="number"
                    value={this.state.numberQuantity}
                    placeholder="Insira aqui"
                    onChange={this.inputHandler}
                  />
                </FormGroup>
              </form>
            </Panel>
          </Col>
        </Row>
        <Row>
          <Col xs={12} md={12}>
            <Table>
              <thead>
                <tr>
                  <th>Tradução</th>
                </tr>
              </thead>
              <tbody>
                {rows}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Grid>


    );
  }
}

export default App;
